# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0009_auto_20160210_0204'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='refresh',
            field=models.BooleanField(default=False),
        ),
    ]
