# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0011_dbinstance'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dbinstance',
            name='allocated_storage',
            field=models.CharField(max_length=255),
        ),
    ]
