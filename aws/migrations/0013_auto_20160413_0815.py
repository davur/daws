# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0012_auto_20160223_0148'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoadBalancer',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('dns_name', models.CharField(max_length=255)),
                ('canonical_hosted_zone_name', models.CharField(max_length=255)),
                ('canonical_hosted_zone_name_id', models.CharField(max_length=255)),
                ('profile', models.ForeignKey(null=True, to='aws.Profile', blank=True, on_delete=models.CASCADE)),
                ('vpc', models.ForeignKey(null=True, to='aws.Vpc', blank=True, on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterField(
            model_name='dbinstance',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[('us-east-1a', 'N. Virginia (us-east-1a)'), ('us-east-1b', 'N. Virginia (us-east-1b)'), ('us-east-1c', 'N. Virginia (us-east-1c)'), ('us-east-1d', 'N. Virginia (us-east-1d)'), ('us-east-1e', 'N. Virginia (us-east-1e)'), ('us-west-2a', 'Oregon (us-west-2a)'), ('us-west-2b', 'Oregon (us-west-2b)'), ('us-west-2c', 'Oregon (us-west-2c)'), ('us-west-1a', 'N. California (us-west-1a)'), ('us-west-1b', 'N. California (us-west-1b)'), ('us-west-1c', 'N. California (us-west-1c)'), ('eu-west-1a', 'Ireland (eu-west-1a)'), ('eu-west-1b', 'Ireland (eu-west-1b)'), ('eu-west-1c', 'Ireland (eu-west-1c)'), ('eu-central-1a', 'Frankfurt (eu-central-1a)'), ('eu-central-1b', 'Frankfurt (eu-central-1b)'), ('eu-central-1c', 'Frankfurt (eu-central-1c)'), ('eu-central-1d', 'Frankfurt (eu-central-1d)'), ('eu-central-1e', 'Frankfurt (eu-central-1e)'), ('eu-central-1f', 'Frankfurt (eu-central-1f)'), ('ap-southeast-1a', 'Singapore (ap-southeast-1a)'), ('ap-southeast-1b', 'Singapore (ap-southeast-1b)'), ('ap-northeast-1a', 'Tokyo (ap-northeast-1a)'), ('ap-northeast-1b', 'Tokyo (ap-northeast-1b)'), ('ap-northeast-1c', 'Tokyo (ap-northeast-1c)'), ('ap-southeast-2a', 'Sydney (ap-southeast-2a)'), ('ap-southeast-2b', 'Sydney (ap-southeast-2b)'), ('ap-northeast-2a', 'Seoul (ap-northeast-2a)'), ('ap-northeast-2b', 'Seoul (ap-northeast-2b)'), ('sa-east-1a', 'Sao Paulo (sa-east-1a)'), ('sa-east-1b', 'Sao Paulo (sa-east-1b)')]),
        ),
        migrations.AlterField(
            model_name='ec2instance',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[('us-east-1a', 'N. Virginia (us-east-1a)'), ('us-east-1b', 'N. Virginia (us-east-1b)'), ('us-east-1c', 'N. Virginia (us-east-1c)'), ('us-east-1d', 'N. Virginia (us-east-1d)'), ('us-east-1e', 'N. Virginia (us-east-1e)'), ('us-west-2a', 'Oregon (us-west-2a)'), ('us-west-2b', 'Oregon (us-west-2b)'), ('us-west-2c', 'Oregon (us-west-2c)'), ('us-west-1a', 'N. California (us-west-1a)'), ('us-west-1b', 'N. California (us-west-1b)'), ('us-west-1c', 'N. California (us-west-1c)'), ('eu-west-1a', 'Ireland (eu-west-1a)'), ('eu-west-1b', 'Ireland (eu-west-1b)'), ('eu-west-1c', 'Ireland (eu-west-1c)'), ('eu-central-1a', 'Frankfurt (eu-central-1a)'), ('eu-central-1b', 'Frankfurt (eu-central-1b)'), ('eu-central-1c', 'Frankfurt (eu-central-1c)'), ('eu-central-1d', 'Frankfurt (eu-central-1d)'), ('eu-central-1e', 'Frankfurt (eu-central-1e)'), ('eu-central-1f', 'Frankfurt (eu-central-1f)'), ('ap-southeast-1a', 'Singapore (ap-southeast-1a)'), ('ap-southeast-1b', 'Singapore (ap-southeast-1b)'), ('ap-northeast-1a', 'Tokyo (ap-northeast-1a)'), ('ap-northeast-1b', 'Tokyo (ap-northeast-1b)'), ('ap-northeast-1c', 'Tokyo (ap-northeast-1c)'), ('ap-southeast-2a', 'Sydney (ap-southeast-2a)'), ('ap-southeast-2b', 'Sydney (ap-southeast-2b)'), ('ap-northeast-2a', 'Seoul (ap-northeast-2a)'), ('ap-northeast-2b', 'Seoul (ap-northeast-2b)'), ('sa-east-1a', 'Sao Paulo (sa-east-1a)'), ('sa-east-1b', 'Sao Paulo (sa-east-1b)')]),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[('us-east-1a', 'N. Virginia (us-east-1a)'), ('us-east-1b', 'N. Virginia (us-east-1b)'), ('us-east-1c', 'N. Virginia (us-east-1c)'), ('us-east-1d', 'N. Virginia (us-east-1d)'), ('us-east-1e', 'N. Virginia (us-east-1e)'), ('us-west-2a', 'Oregon (us-west-2a)'), ('us-west-2b', 'Oregon (us-west-2b)'), ('us-west-2c', 'Oregon (us-west-2c)'), ('us-west-1a', 'N. California (us-west-1a)'), ('us-west-1b', 'N. California (us-west-1b)'), ('us-west-1c', 'N. California (us-west-1c)'), ('eu-west-1a', 'Ireland (eu-west-1a)'), ('eu-west-1b', 'Ireland (eu-west-1b)'), ('eu-west-1c', 'Ireland (eu-west-1c)'), ('eu-central-1a', 'Frankfurt (eu-central-1a)'), ('eu-central-1b', 'Frankfurt (eu-central-1b)'), ('eu-central-1c', 'Frankfurt (eu-central-1c)'), ('eu-central-1d', 'Frankfurt (eu-central-1d)'), ('eu-central-1e', 'Frankfurt (eu-central-1e)'), ('eu-central-1f', 'Frankfurt (eu-central-1f)'), ('ap-southeast-1a', 'Singapore (ap-southeast-1a)'), ('ap-southeast-1b', 'Singapore (ap-southeast-1b)'), ('ap-northeast-1a', 'Tokyo (ap-northeast-1a)'), ('ap-northeast-1b', 'Tokyo (ap-northeast-1b)'), ('ap-northeast-1c', 'Tokyo (ap-northeast-1c)'), ('ap-southeast-2a', 'Sydney (ap-southeast-2a)'), ('ap-southeast-2b', 'Sydney (ap-southeast-2b)'), ('ap-northeast-2a', 'Seoul (ap-northeast-2a)'), ('ap-northeast-2b', 'Seoul (ap-northeast-2b)'), ('sa-east-1a', 'Sao Paulo (sa-east-1a)'), ('sa-east-1b', 'Sao Paulo (sa-east-1b)')]),
        ),
    ]
