# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0004_ec2instance'),
    ]

    operations = [
        migrations.AddField(
            model_name='ec2instance',
            name='profile',
            field=models.ForeignKey(to='aws.Profile', null=True, blank=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='subnet',
            name='profile',
            field=models.ForeignKey(to='aws.Profile', null=True, blank=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='vpc',
            name='profile',
            field=models.ForeignKey(to='aws.Profile', null=True, blank=True, on_delete=models.CASCADE),
        ),
    ]
