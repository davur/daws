# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subnet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('subnet_id', models.CharField(max_length=255)),
                ('state', models.CharField(max_length=255)),
                ('cidr', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterModelOptions(
            name='vpc',
            options={'ordering': ['name'], 'verbose_name': 'VPC'},
        ),
        migrations.RenameField(
            model_name='vpc',
            old_name='vpc_cidr',
            new_name='cidr',
        ),
        migrations.AddField(
            model_name='subnet',
            name='vpc',
            field=models.ForeignKey(to='aws.Vpc', on_delete=models.CASCADE),
        ),
    ]
