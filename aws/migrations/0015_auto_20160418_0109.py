# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0014_auto_20160418_0107'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dbinstance',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
        migrations.AlterField(
            model_name='ec2instance',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
        migrations.AlterField(
            model_name='loadbalancer',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
        migrations.AlterField(
            model_name='vpc',
            name='refreshed_time',
            field=models.DateTimeField(default=django.utils.timezone.now, blank=True),
        ),
    ]
