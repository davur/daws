# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0013_auto_20160413_0815'),
    ]

    operations = [
        migrations.AddField(
            model_name='dbinstance',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='ec2instance',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='loadbalancer',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='subnet',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
        migrations.AddField(
            model_name='vpc',
            name='refreshed_time',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
