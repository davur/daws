# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0015_auto_20160418_0109'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subnet',
            options={'ordering': ['availability_zone', 'name']},
        ),
        migrations.AlterField(
            model_name='dbinstance',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[(b'us-east-1a', b'N. Virginia (us-east-1a)'), (b'us-east-1b', b'N. Virginia (us-east-1b)'), (b'us-east-1c', b'N. Virginia (us-east-1c)'), (b'us-east-1d', b'N. Virginia (us-east-1d)'), (b'us-east-1e', b'N. Virginia (us-east-1e)'), (b'us-west-2a', b'Oregon (us-west-2a)'), (b'us-west-2b', b'Oregon (us-west-2b)'), (b'us-west-2c', b'Oregon (us-west-2c)'), (b'us-west-1a', b'N. California (us-west-1a)'), (b'us-west-1b', b'N. California (us-west-1b)'), (b'us-west-1c', b'N. California (us-west-1c)'), (b'eu-west-1a', b'Ireland (eu-west-1a)'), (b'eu-west-1b', b'Ireland (eu-west-1b)'), (b'eu-west-1c', b'Ireland (eu-west-1c)'), (b'eu-central-1a', b'Frankfurt (eu-central-1a)'), (b'eu-central-1b', b'Frankfurt (eu-central-1b)'), (b'eu-central-1c', b'Frankfurt (eu-central-1c)'), (b'eu-central-1d', b'Frankfurt (eu-central-1d)'), (b'eu-central-1e', b'Frankfurt (eu-central-1e)'), (b'eu-central-1f', b'Frankfurt (eu-central-1f)'), (b'ap-southeast-1a', b'Singapore (ap-southeast-1a)'), (b'ap-southeast-1b', b'Singapore (ap-southeast-1b)'), (b'ap-northeast-1a', b'Tokyo (ap-northeast-1a)'), (b'ap-northeast-1b', b'Tokyo (ap-northeast-1b)'), (b'ap-northeast-1c', b'Tokyo (ap-northeast-1c)'), (b'ap-southeast-2a', b'Sydney (ap-southeast-2a)'), (b'ap-southeast-2b', b'Sydney (ap-southeast-2b)'), (b'ap-southeast-2c', b'Sydney (ap-southeast-2c)'), (b'ap-northeast-2a', b'Seoul (ap-northeast-2a)'), (b'ap-northeast-2b', b'Seoul (ap-northeast-2b)'), (b'sa-east-1a', b'Sao Paulo (sa-east-1a)'), (b'sa-east-1b', b'Sao Paulo (sa-east-1b)')]),
        ),
        migrations.AlterField(
            model_name='ec2instance',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[(b'us-east-1a', b'N. Virginia (us-east-1a)'), (b'us-east-1b', b'N. Virginia (us-east-1b)'), (b'us-east-1c', b'N. Virginia (us-east-1c)'), (b'us-east-1d', b'N. Virginia (us-east-1d)'), (b'us-east-1e', b'N. Virginia (us-east-1e)'), (b'us-west-2a', b'Oregon (us-west-2a)'), (b'us-west-2b', b'Oregon (us-west-2b)'), (b'us-west-2c', b'Oregon (us-west-2c)'), (b'us-west-1a', b'N. California (us-west-1a)'), (b'us-west-1b', b'N. California (us-west-1b)'), (b'us-west-1c', b'N. California (us-west-1c)'), (b'eu-west-1a', b'Ireland (eu-west-1a)'), (b'eu-west-1b', b'Ireland (eu-west-1b)'), (b'eu-west-1c', b'Ireland (eu-west-1c)'), (b'eu-central-1a', b'Frankfurt (eu-central-1a)'), (b'eu-central-1b', b'Frankfurt (eu-central-1b)'), (b'eu-central-1c', b'Frankfurt (eu-central-1c)'), (b'eu-central-1d', b'Frankfurt (eu-central-1d)'), (b'eu-central-1e', b'Frankfurt (eu-central-1e)'), (b'eu-central-1f', b'Frankfurt (eu-central-1f)'), (b'ap-southeast-1a', b'Singapore (ap-southeast-1a)'), (b'ap-southeast-1b', b'Singapore (ap-southeast-1b)'), (b'ap-northeast-1a', b'Tokyo (ap-northeast-1a)'), (b'ap-northeast-1b', b'Tokyo (ap-northeast-1b)'), (b'ap-northeast-1c', b'Tokyo (ap-northeast-1c)'), (b'ap-southeast-2a', b'Sydney (ap-southeast-2a)'), (b'ap-southeast-2b', b'Sydney (ap-southeast-2b)'), (b'ap-southeast-2c', b'Sydney (ap-southeast-2c)'), (b'ap-northeast-2a', b'Seoul (ap-northeast-2a)'), (b'ap-northeast-2b', b'Seoul (ap-northeast-2b)'), (b'sa-east-1a', b'Sao Paulo (sa-east-1a)'), (b'sa-east-1b', b'Sao Paulo (sa-east-1b)')]),
        ),
        migrations.AlterField(
            model_name='subnet',
            name='availability_zone',
            field=models.CharField(max_length=255, choices=[(b'us-east-1a', b'N. Virginia (us-east-1a)'), (b'us-east-1b', b'N. Virginia (us-east-1b)'), (b'us-east-1c', b'N. Virginia (us-east-1c)'), (b'us-east-1d', b'N. Virginia (us-east-1d)'), (b'us-east-1e', b'N. Virginia (us-east-1e)'), (b'us-west-2a', b'Oregon (us-west-2a)'), (b'us-west-2b', b'Oregon (us-west-2b)'), (b'us-west-2c', b'Oregon (us-west-2c)'), (b'us-west-1a', b'N. California (us-west-1a)'), (b'us-west-1b', b'N. California (us-west-1b)'), (b'us-west-1c', b'N. California (us-west-1c)'), (b'eu-west-1a', b'Ireland (eu-west-1a)'), (b'eu-west-1b', b'Ireland (eu-west-1b)'), (b'eu-west-1c', b'Ireland (eu-west-1c)'), (b'eu-central-1a', b'Frankfurt (eu-central-1a)'), (b'eu-central-1b', b'Frankfurt (eu-central-1b)'), (b'eu-central-1c', b'Frankfurt (eu-central-1c)'), (b'eu-central-1d', b'Frankfurt (eu-central-1d)'), (b'eu-central-1e', b'Frankfurt (eu-central-1e)'), (b'eu-central-1f', b'Frankfurt (eu-central-1f)'), (b'ap-southeast-1a', b'Singapore (ap-southeast-1a)'), (b'ap-southeast-1b', b'Singapore (ap-southeast-1b)'), (b'ap-northeast-1a', b'Tokyo (ap-northeast-1a)'), (b'ap-northeast-1b', b'Tokyo (ap-northeast-1b)'), (b'ap-northeast-1c', b'Tokyo (ap-northeast-1c)'), (b'ap-southeast-2a', b'Sydney (ap-southeast-2a)'), (b'ap-southeast-2b', b'Sydney (ap-southeast-2b)'), (b'ap-southeast-2c', b'Sydney (ap-southeast-2c)'), (b'ap-northeast-2a', b'Seoul (ap-northeast-2a)'), (b'ap-northeast-2b', b'Seoul (ap-northeast-2b)'), (b'sa-east-1a', b'Sao Paulo (sa-east-1a)'), (b'sa-east-1b', b'Sao Paulo (sa-east-1b)')]),
        ),
    ]
