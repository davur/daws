# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0006_auto_20160210_0056'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='notes',
            field=models.TextField(blank=True),
        ),
    ]
