# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0010_profile_refresh'),
    ]

    operations = [
        migrations.CreateModel(
            name='DbInstance',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('dbi_resource_id', models.CharField(max_length=255, unique=True)),
                ('engine', models.CharField(max_length=255)),
                ('engine_version', models.CharField(max_length=255)),
                ('status', models.CharField(max_length=255)),
                ('instance_class', models.CharField(max_length=255)),
                ('multi_az', models.BooleanField(default=False)),
                ('publicly_accessible', models.BooleanField(default=False)),
                ('allocated_storage', models.BooleanField(default=False)),
                ('auto_minor_version_upgrade', models.BooleanField(default=False)),
                ('availability_zone', models.CharField(max_length=255)),
                ('preferred_backup_window', models.CharField(max_length=255)),
                ('preferred_maintenance_window', models.CharField(max_length=255)),
                ('profile', models.ForeignKey(blank=True, to='aws.Profile', null=True, on_delete=models.CASCADE)),
                ('vpc', models.ForeignKey(blank=True, to='aws.Vpc', null=True, on_delete=models.CASCADE)),
            ],
        ),
    ]
