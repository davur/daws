# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0002_auto_20160114_0138'),
    ]

    operations = [
        migrations.AddField(
            model_name='subnet',
            name='availability_zone',
            field=models.CharField(default='na', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='subnet',
            name='subnet_id',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='vpc',
            name='vpc_id',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
