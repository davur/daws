# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0003_auto_20160114_0144'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ec2Instance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('instance_id', models.CharField(unique=True, max_length=255)),
                ('state', models.CharField(max_length=255)),
                ('private_dns_name', models.CharField(max_length=255)),
                ('private_ip_address', models.CharField(max_length=255)),
                ('public_dns_name', models.CharField(max_length=255)),
                ('public_ip_address', models.CharField(max_length=255)),
                ('key_name', models.CharField(max_length=255)),
                ('instance_type', models.CharField(max_length=255)),
                ('availability_zone', models.CharField(max_length=255)),
                ('reservation_id', models.CharField(max_length=255)),
                ('subnet', models.ForeignKey(null=True, blank=True, to='aws.Subnet', on_delete=models.CASCADE)),
                ('vpc', models.ForeignKey(null=True, blank=True, to='aws.Vpc', on_delete=models.CASCADE)),
            ],
        ),
    ]
