# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aws', '0007_profile_notes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='account_email',
        ),
        migrations.RemoveField(
            model_name='profile',
            name='account_number',
        ),
    ]
