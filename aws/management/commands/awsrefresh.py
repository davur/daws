from django.core.management.base import BaseCommand, CommandError

import boto3
from django.utils import timezone

from aws.models import *

class Command(BaseCommand):
    help = 'Scans the registered AWS profiles for new resources'

    profile = None
    region = None
    ec2 = None

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_id', nargs='+', type=int)
    #
    def handle(self, *args, **options):
        profiles = Profile.objects.filter(refresh=True)
        # profiles = Profile.objects.filter(refresh=True, name='test')

        regions = []
        # Use a custom regions array to scan desired regions only
        # regions = [
        #     'us-east-1',
        #     'us-west-2',
        #     'us-west-1',
        #     'eu-west-1',
        #     'eu-central-1',
        #     'ap-southeast-1',
        #     'ap-northeast-1',
        #     'ap-southeast-2',
        #     'ap-northeast-2',
        #     'sa-east-1',
        # ]

        for profile in profiles:
            self.profile = profile
            self.stdout.write("Profile '%s'" % profile)
            boto3.setup_default_session(profile_name=profile.name)


            if not regions:
                client = boto3.client('ec2')
                regions_response = client.describe_regions(
                    DryRun=False
                )

                for region_obj in regions_response['Regions']:
                    region = region_obj['RegionName']
                    regions.append(region)


            for region in regions:
                self.region = region
                self.stdout.write("Region '%s'" % region)

                self.ec2 = boto3.client('ec2', region_name=region)

                self.refresh_vpcs()

                self.refresh_subnets()

                self.refresh_ec2()

                self.refresh_rds()

            # describe_load_balancers does not filter by region,
            # seems odd, but leaving for later investigation
            self.refresh_loadbalancers()

    def refresh_security_groups(self):
        ec2 = self.ec2
        profile = self.profile

        self.stdout.write("Security Groups:")
        response = ec2.describe_security_groups(
            DryRun=False,
        )
        for sg in response['SecurityGroups']:
            self.stdout.write(sg['GroupId'])
            obj, created = SecurityGroup.objects.get_or_create(group_id=sg['GroupId'])

            if created:
                self.stdout.write("Created")

            # TODO


    def refresh_vpcs(self):
        ec2 = self.ec2
        profile = self.profile

        self.stdout.write("VPCs:")
        response = ec2.describe_vpcs(
            DryRun=False,
        )
        for vpc in response['Vpcs']:
            self.stdout.write(vpc['VpcId'])

            obj, created = Vpc.objects.get_or_create(vpc_id=vpc['VpcId'])

            if created:
                self.stdout.write('Created')

            obj.state = vpc['State']
            obj.cidr = vpc['CidrBlock']
            obj.profile = profile

            if 'Tags' in vpc:
                for tag in vpc['Tags']:
                    if tag['Key'] == 'Name':
                        obj.name = tag['Value']

            obj.refreshed_time = timezone.now()
            obj.save()

    def refresh_loadbalancers(self):
        profile = self.profile
        region = self.region

        self.stdout.write("Load Balancers:")
        client = boto3.client('elb') #, region_name=region)
        response = client.describe_load_balancers()

        for elb in response['LoadBalancerDescriptions']:
            self.stdout.write(elb['DNSName'])

            obj, created = LoadBalancer.objects.get_or_create(dns_name=elb['DNSName'])

            if created:
                self.stdout.write('Created')

            obj.name = elb['LoadBalancerName']
            if 'CanonicalHostedZoneName' in elb:
                obj.canonical_hosted_zone_name = elb['CanonicalHostedZoneName']
            else:
                obj.canonical_hosted_zone_name = ''

            if 'CanonicalHostedZoneNameID' in elb:
                obj.canonical_hosted_zone_name_id = elb['CanonicalHostedZoneNameID']
            else:
                obj.canonical_hosted_zone_name_id = ''

            if ('VPCId' in elb) and elb['VPCId']:
                vpc = Vpc.objects.get(vpc_id=elb['VPCId'])
                obj.vpc = vpc

            obj.profile = profile

            obj.refreshed_time = timezone.now()
            obj.save()



    def refresh_subnets(self):
        ec2 = self.ec2
        profile = self.profile

        self.stdout.write("Subnets:")
        response = ec2.describe_subnets(
            DryRun=False,
        )
        for subnet in response['Subnets']:
            self.stdout.write(subnet['SubnetId'])

            vpc = Vpc.objects.get(vpc_id=subnet['VpcId'])
            obj, created = Subnet.objects.get_or_create(subnet_id=subnet['SubnetId'], vpc=vpc)

            if created:
                self.stdout.write('Created')

            obj.state = subnet['State']
            obj.cidr = subnet['CidrBlock']
            obj.availability_zone = subnet['AvailabilityZone']
            obj.profile = profile

            if 'Tags' in subnet:
                for tag in subnet['Tags']:
                    if tag['Key'] == 'Name':
                        obj.name = tag['Value']

            obj.refreshed_time = timezone.now()
            obj.save()


    def refresh_ec2(self):
        ec2 = self.ec2
        profile = self.profile

        self.stdout.write("EC2 Instances:")
        response = ec2.describe_instances(
            DryRun=False,
        )
        for reservation in response['Reservations']:
            reservation_id = reservation['ReservationId']

            for instance in reservation['Instances']:
                self.stdout.write(instance['InstanceId'])

                obj, created = Ec2Instance.objects.get_or_create(instance_id=instance['InstanceId'])

                if created:
                    self.stdout.write('Created')

                if 'VpcId' in instance:
                    vpc = Vpc.objects.get(vpc_id=instance['VpcId'])
                    obj.vpc = vpc

                if 'SubnetId' in instance:
                    subnet = Subnet.objects.get(subnet_id=instance['SubnetId'])
                    obj.subnet = subnet

                obj.state = instance['State']['Name']

                if 'PrivateDnsName' in instance:
                    obj.private_dns_name = instance['PrivateDnsName']

                if 'PrivateIpAddress' in instance:
                    obj.private_ip_address = instance['PrivateIpAddress']

                if 'PublicDnsName' in instance:
                    obj.public_dns_name = instance['PublicDnsName']

                obj.profile = profile

                if 'PublicIpAddress' in instance:
                    obj.public_ip_address = instance['PublicIpAddress']

                if 'KeyName' in instance:
                    obj.key_name = instance['KeyName']

                obj.instance_type = instance['InstanceType']
                obj.availability_zone = instance['Placement']['AvailabilityZone']
                obj.reservation_id = reservation_id

                if 'Tags' in instance:
                    for tag in instance['Tags']:
                        if tag['Key'] == 'Name':
                            obj.name = tag['Value']

                obj.refreshed_time = timezone.now()
                obj.save()

    def refresh_rds(self):
        profile = self.profile
        region = self.region

        self.stdout.write("RDS Instances:")
        rds = boto3.client('rds', region_name=region)
        response = rds.describe_db_instances()

        for db_instance in response['DBInstances']:
            self.stdout.write(db_instance['DbiResourceId'])

            obj, created = DbInstance.objects.get_or_create(dbi_resource_id=db_instance['DbiResourceId'])

            if created:
                self.stdout.write('Created')


            obj.name = db_instance['DBInstanceIdentifier']
            obj.engine = db_instance['Engine']
            obj.engine_version = db_instance['EngineVersion']
            obj.status = db_instance['DBInstanceStatus']
            obj.instance_class = db_instance['DBInstanceClass']
            obj.multi_az = db_instance['MultiAZ']
            obj.publicly_accessible = db_instance['PubliclyAccessible']
            obj.allocated_storage = db_instance['AllocatedStorage']
            obj.auto_minor_version_upgrade = db_instance['AutoMinorVersionUpgrade']
            obj.availability_zone = db_instance['AvailabilityZone']
            obj.preferred_backup_window = db_instance['PreferredBackupWindow']
            obj.preferred_maintenance_window = db_instance['PreferredMaintenanceWindow']


            vpc_id = db_instance['DBSubnetGroup']['VpcId']
            vpc = Vpc.objects.get(vpc_id=vpc_id)
            obj.vpc = vpc

            obj.profile = profile

            obj.refreshed_time = timezone.now()
            obj.save()
