from django.contrib import admin

from .models import *

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'refresh',
        'account_name',
        'account_number',
        'notes',
        'refreshed_time',
    ]
    pass

@admin.register(Vpc)
class VpcAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'profile',
        'vpc_id',
        'state',
        'cidr',
        'refreshed_time',
    ]

@admin.register(Subnet)
class SubnetAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'profile',
        'subnet_id',
        'vpc',
        'state',
        'cidr',
        'availability_zone',
        'refreshed_time',
    ]

@admin.register(Ec2Instance)
class Ec2Admin(admin.ModelAdmin):
    list_display = [
        'name',
        'profile',
        'instance_id',
        'instance_type',
        'availability_zone',
        'state',
        'connection',
        'refreshed_time',
    ]
    list_filter = [
        'profile',
        'availability_zone',
        'state',
        'instance_type',
    ]
    search_fields = [
        'name',
        'profile__name',
        'private_dns_name',
        'private_ip_address',
        'public_dns_name',
        'public_ip_address',
        'key_name',
    ]

@admin.register(DbInstance)
class DbInstanceAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'profile',
        'availability_zone',
        'engine',
        'status',
        'instance_class',
        'refreshed_time',
    ]

    list_filter = [
        'profile',
        'availability_zone',
        'status',
        'instance_class',
        'engine',
        'engine_version',
    ]
    search_fields = [
        'name',
        'profile__name',
    ]

@admin.register(LoadBalancer)
class LoadBalancerAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'dns_name',
        'profile',
        'vpc',
        'refreshed_time',
    ]
    search_fields = [
        'name',
        'dns_name',
        'profile__name',
        'vpc__name',
        'vpc__vpc_id',
    ]
    list_filter = [
        'profile',
        'vpc'
    ]
