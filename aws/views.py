from django.shortcuts import render

from django.views.generic import ListView, TemplateView
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView


from .models import *



class BaseObjectListView(ListView):
    template_name = "base_object_list.html"

class BaseObjectDetailView(DetailView):
    template_name = "base_object_detail.html"

class ProfileListView(BaseObjectListView):
    model = Profile

class ProfileDetailView(BaseObjectDetailView):
    model = Profile


class VpcListView(BaseObjectListView):
    model = Vpc

class VpcDetailView(BaseObjectDetailView):
    model = Vpc

class SubnetDetailView(BaseObjectDetailView):
    model = Subnet

class Ec2InstanceDetailView(BaseObjectDetailView):
    model = Ec2Instance

class DbInstanceDetailView(BaseObjectDetailView):
    model = DbInstance


class VpcGraphicView(DetailView):
    model = Vpc
    template_name = "aws/vpc_graphic_view.html"
