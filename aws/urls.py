from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^vpc$', VpcListView.as_view(), name='vpc-list'),
    url(r'^vpc/(?P<pk>\d+)/$', VpcDetailView.as_view(), name='crud-Vpc-detail'),
    url(r'^vpc/(?P<pk>\d+)/graphic/$', VpcGraphicView.as_view(), name='graphic-Vpc-detail'),
    url(r'^$', ProfileListView.as_view(), name='profile-list'),
    url(r'^profile/(?P<pk>\d+)/$', ProfileDetailView.as_view(), name='crud-Profile-detail'),
    url(r'^subnet/(?P<pk>\d+)/$', SubnetDetailView.as_view(), name='crud-Subnet-detail'),
    url(r'^ec2/(?P<pk>\d+)/$', Ec2InstanceDetailView.as_view(), name='crud-Ec2Instance-detail'),
    url(r'^rds/(?P<pk>\d+)/$', DbInstanceDetailView.as_view(), name='crud-DbInstance-detail'),
]
