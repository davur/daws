from django.db import models
from django.utils import timezone
from django.urls import reverse


# Create your models here.

AVAILABILITY_ZONE_CHOICES = [
    ('us-east-1a', 'N. Virginia (us-east-1a)'),
    ('us-east-1b', 'N. Virginia (us-east-1b)'),
    ('us-east-1c', 'N. Virginia (us-east-1c)'),
    ('us-east-1d', 'N. Virginia (us-east-1d)'),
    ('us-east-1e', 'N. Virginia (us-east-1e)'),
    ('us-west-2a', 'Oregon (us-west-2a)'),
    ('us-west-2b', 'Oregon (us-west-2b)'),
    ('us-west-2c', 'Oregon (us-west-2c)'),
    ('us-west-1a', 'N. California (us-west-1a)'),
    ('us-west-1b', 'N. California (us-west-1b)'),
    ('us-west-1c', 'N. California (us-west-1c)'),
    ('eu-west-1a', 'Ireland (eu-west-1a)'),
    ('eu-west-1b', 'Ireland (eu-west-1b)'),
    ('eu-west-1c', 'Ireland (eu-west-1c)'),
    ('eu-central-1a', 'Frankfurt (eu-central-1a)'),
    ('eu-central-1b', 'Frankfurt (eu-central-1b)'),
    ('eu-central-1c', 'Frankfurt (eu-central-1c)'),
    ('eu-central-1d', 'Frankfurt (eu-central-1d)'),
    ('eu-central-1e', 'Frankfurt (eu-central-1e)'),
    ('eu-central-1f', 'Frankfurt (eu-central-1f)'),
    ('ap-southeast-1a', 'Singapore (ap-southeast-1a)'),
    ('ap-southeast-1b', 'Singapore (ap-southeast-1b)'),
    ('ap-northeast-1a', 'Tokyo (ap-northeast-1a)'),
    ('ap-northeast-1b', 'Tokyo (ap-northeast-1b)'),
    ('ap-northeast-1c', 'Tokyo (ap-northeast-1c)'),
    ('ap-southeast-2a', 'Sydney (ap-southeast-2a)'),
    ('ap-southeast-2b', 'Sydney (ap-southeast-2b)'),
    ('ap-southeast-2c', 'Sydney (ap-southeast-2c)'),
    ('ap-northeast-2a', 'Seoul (ap-northeast-2a)'),
    ('ap-northeast-2b', 'Seoul (ap-northeast-2b)'),
    ('sa-east-1a', 'Sao Paulo (sa-east-1a)'),
    ('sa-east-1b', 'Sao Paulo (sa-east-1b)'),
]


class BaseModel():
    def __str__(self):
        if not self.name:
            return 'Untitled %s' % (self.__class__.__name__)
        return self.name

    def get_absolute_url(self):
        return reverse("crud-%s-detail" % (self.__class__.__name__), args=(self.id,))

    def get_fields(self):
        fields = []
        for field in self.__class__._meta.fields:
            fields.append((field.name, getattr(self, field.name), field.is_relation))

        # return [(field.name, field.value_from_object(self)) for field in self.__class__._meta.fields]
        return fields



class Profile(BaseModel, models.Model):
    name = models.CharField(max_length=255)

    refresh = models.BooleanField(default=False)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    account_name = models.CharField(max_length=255, blank=True, null=False)
    account_email = models.CharField(max_length=255, blank=True, null=False)
    account_number = models.CharField(max_length=255, blank=True, null=False)

    notes = models.TextField(blank=True, null=False)

    class Meta:
        ordering = ['name',]

    def __str__(self):
        return self.name


    def get_links(self):
        return [
            ("VPCs", self.vpc_set.order_by('-name').all())
        ]

class Vpc(BaseModel, models.Model):
    name = models.CharField(max_length=255)
    vpc_id = models.CharField(max_length=255, unique=True)
    state = models.CharField(max_length=255)
    cidr = models.CharField(max_length=255)
    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.CASCADE)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    class Meta:
        ordering = ['name',]
        verbose_name = 'VPC'

    def __str__(self):
        return '"%s" - %s (%s)' % (self.name, self.cidr, self.vpc_id)

    def get_links(self):
        return [
            ("Subnets", self.subnet_set.order_by("-name").all()),
            ("EC2 Instances", self.ec2instance_set.order_by('-name').all()),
            ("RDS Instances", self.dbinstance_set.order_by('-name').all()),
        ]


class Subnet(BaseModel, models.Model):
    class Meta:
        ordering = ['availability_zone', 'name',]

    name = models.CharField(max_length=255)
    subnet_id = models.CharField(max_length=255, unique=True)
    state = models.CharField(max_length=255)
    cidr = models.CharField(max_length=255)

    vpc = models.ForeignKey(Vpc, on_delete=models.CASCADE)
    availability_zone = models.CharField(max_length=255, choices=AVAILABILITY_ZONE_CHOICES)

    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.CASCADE)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    def __str__(self):
        return '"%s" - %s (%s)' % (self.name, self.cidr, self.subnet_id)

    def get_links(self):
        return [
            ("EC2 Instances", self.ec2instance_set.order_by('-name').all()),
        ]


class Ec2Instance(BaseModel, models.Model):
    name = models.CharField(max_length=255)
    instance_id = models.CharField(max_length=255, unique=True)

    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.CASCADE)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    state = models.CharField(max_length=255)
    private_dns_name = models.CharField(max_length=255)
    private_ip_address = models.CharField(max_length=255)
    public_dns_name = models.CharField(max_length=255)
    public_ip_address = models.CharField(max_length=255)
    key_name = models.CharField(max_length=255)
    instance_type = models.CharField(max_length=255)
    availability_zone = models.CharField(max_length=255, choices=AVAILABILITY_ZONE_CHOICES)
    reservation_id = models.CharField(max_length=255)

    subnet = models.ForeignKey(Subnet, blank=True, null=True, on_delete=models.CASCADE)
    vpc = models.ForeignKey(Vpc, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return '"%s" - %s (%s)' % (self.name, self.private_ip_address, self.instance_id)

    def connection(self):
        return 'ssh -i %s.pem ec2-user@%s' % (self.key_name, self.public_ip_address)


class DbInstance(BaseModel, models.Model):
    name = models.CharField(max_length=255)
    dbi_resource_id = models.CharField(max_length=255, unique=True)

    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.CASCADE)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    engine = models.CharField(max_length=255)
    engine_version = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    instance_class = models.CharField(max_length=255)
    vpc = models.ForeignKey(Vpc, blank=True, null=True, on_delete=models.CASCADE)
    multi_az = models.BooleanField(default=False)
    publicly_accessible = models.BooleanField(default=False)
    allocated_storage = models.CharField(max_length=255)
    auto_minor_version_upgrade = models.BooleanField(default=False)
    availability_zone = models.CharField(max_length=255, choices=AVAILABILITY_ZONE_CHOICES)

    preferred_backup_window = models.CharField(max_length=255)
    preferred_maintenance_window = models.CharField(max_length=255)

    def __str__(self):
        return '"%s" (%s)' % (self.name, self.dbi_resource_id)


class LoadBalancer(models.Model):
    name = models.CharField(max_length=255)
    dns_name = models.CharField(max_length=255)

    profile = models.ForeignKey(Profile, blank=True, null=True, on_delete=models.CASCADE)
    refreshed_time = models.DateTimeField(default=timezone.now, blank=True)

    canonical_hosted_zone_name = models.CharField(max_length=255)
    canonical_hosted_zone_name_id = models.CharField(max_length=255)
    vpc = models.ForeignKey(Vpc, blank=True, null=True, on_delete=models.CASCADE)

# OptionGroupMemberships: [{'OptionGroupName': 'default:postgres-9-4', 'Status': 'in-sync'}]
# PreferredBackupWindow: 12:10-12:40
# CopyTagsToSnapshot: False
# DbiResourceId: db-OY53HM6AG325T6ADRKBDGBYO4Q
# DBParameterGroups: [{'ParameterApplyStatus': 'in-sync', 'DBParameterGroupName': 'default.postgres9.4'}]
# EngineVersion: 9.4.4
# DBName: ecofield_api
# PubliclyAccessible: True
# LicenseModel: postgresql-license
# AllocatedStorage: 5
# PendingModifiedValues: {}
# Endpoint: {'Port': 5432, 'Address': 'docker.czqifdpblerm.ap-southeast-2.rds.amazonaws.com'}
# AutoMinorVersionUpgrade: True
# MultiAZ: False
# DbInstancePort: 0
# MonitoringInterval: 0
# AvailabilityZone: ap-southeast-2a
# DBSubnetGroup: {'SubnetGroupStatus': 'Complete', 'DBSubnetGroupDescription': 'Testing Sydney VPC Subnet Group', 'Subnets': [{'SubnetAvailabilityZone': {'Name': 'ap-southeast-2b'}, 'SubnetIdentifier': 'subnet-9ec0d5ea', 'SubnetStatus': 'Active'}, {'SubnetAvailabilityZone': {'Name': 'ap-southeast-2a'}, 'SubnetIdentifier': 'subnet-b0977ed5', 'SubnetStatus': 'Active'}], 'VpcId': 'vpc-b17297d4', 'DBSubnetGroupName': 'test-syd'}
# BackupRetentionPeriod: 7
# PreferredMaintenanceWindow: sun:15:38-sun:16:08
# MasterUsername: root
# LatestRestorableTime: 2016-02-23 00:48:42+00:00
# VpcSecurityGroups: [{'VpcSecurityGroupId': 'sg-89a6ecec', 'Status': 'active'}]
# InstanceCreateTime: 2015-10-13 00:59:06.705000+00:00
# DBInstanceIdentifier: docker
# StorageType: standard
# CACertificateIdentifier: rds-ca-2015
# ReadReplicaDBInstanceIdentifiers: []
# DBInstanceClass: db.t2.micro
# StorageEncrypted: False
# Engine: postgres
# DBInstanceStatus: available
# DBSecurityGroups: []
