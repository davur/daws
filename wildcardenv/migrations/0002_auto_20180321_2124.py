# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wildcardenv', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='environment',
            name='stack_id',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AlterField(
            model_name='environment',
            name='template',
            field=models.ForeignKey(blank=True, null=True, related_name='environments', to='wildcardenv.Template', on_delete=models.CASCADE),
        ),
    ]
