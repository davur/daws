from django.shortcuts import redirect
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse

from django.views.generic import ListView, TemplateView
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView

from .models import *

class BaseObjectListView(ListView):
    template_name = "base_object_list.html"

class BaseObjectDetailView(DetailView):
    template_name = "base_object_detail.html"


class TemplateListView(BaseObjectListView):
    model = Template

class TemplateDetailView(BaseObjectDetailView):
    model = Template

class EnvironmentListView(BaseObjectListView):
    model = Environment

class EnvironmentDetailView(BaseObjectDetailView):
    template_name = "wildcardenv/env.html"
    model = Environment



def check_env(request, pk):
    import boto3
    boto3.setup_default_session(profile_name='innablr-dev')
    client = boto3.client('cloudformation')

    
def get_client(client):
    import boto3
    boto3.setup_default_session(profile_name='innablr-dev')
    return boto3.client(client)
    


def launch_env(request, pk):
    client = get_client('cloudformation')

    print("blarg")
    
    env = Environment.objects.get(pk=pk)

    client = get_client('cloudformation')

    stack_name = "wildcardenv-%s" % env.name

    if not env.stack_id:
        response = client.create_stack(
               StackName=stack_name,
               TemplateBody=env.template.body,
               Parameters=[
                   {
                       'ParameterKey': 'KeyName',
                       'ParameterValue': 'alex-dev',
                   },
                   {
                       'ParameterKey': 'InstanceType',
                       'ParameterValue': 't2.nano',
                   },
                ],
            )
        if 'StackId' in response:
            env.stack_id = response['StackId']
            env.save()

    return JsonResponse(response)
    # return redirect(env)
    # return HttpResponse(str(response))

def describe_stack(request, pk):
    client = get_client('cloudformation')
    env = Environment.objects.get(pk=pk)
    response = client.describe_stacks(
            StackName=env.stack_id,
            NextToken='1'
        )
    return JsonResponse(response)
