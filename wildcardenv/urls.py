from django.conf.urls import url

from .views import *
from wildcardenv.views import TemplateDetailView

urlpatterns = [
    url(r'^env/$', EnvironmentListView.as_view(), name='env-list'),
    url(r'^env/(?P<pk>\d+)/$', EnvironmentDetailView.as_view(), name='crud-Environment-detail'),
    url(r'^template/$', TemplateListView.as_view(), name='template-list'),
    url(r'^template/(?P<pk>\d+)/$', TemplateDetailView.as_view(), name='crud-Template-detail'),
    url(r'^env/(?P<pk>\d+)/launch/$', launch_env),
    url(r'^env/(?P<pk>\d+)/describe/$', describe_stack),
]
