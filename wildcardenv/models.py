from django.db import models
from django.urls import reverse


class BaseModel():
    def __str__(self):
        if not self.name:
            return 'Untitled %s' % (self.__class__.__name__)
        return self.name

    def get_absolute_url(self):
        return reverse("crud-%s-detail" % (self.__class__.__name__), args=(self.id,))

    def get_fields(self):
        fields = []
        for field in self.__class__._meta.fields:
            fields.append((field.name, getattr(self, field.name), field.is_relation))

        # return [(field.name, field.value_from_object(self)) for field in self.__class__._meta.fields]
        return fields

class Template(BaseModel, models.Model):
    name = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=False)

class Environment(BaseModel, models.Model):
    name = models.CharField(max_length=255)
    template = models.ForeignKey(Template, blank=True, null=True, related_name='environments', on_delete=models.CASCADE)
    stack_id = models.CharField(blank=True, null=False, max_length=32)

