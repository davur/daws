from django.contrib import admin

from .models import *

@admin.register(Template)
class TemplateAdmin(admin.ModelAdmin):
    pass

@admin.register(Environment)
class EnvironmentAdmin(admin.ModelAdmin):
    pass
