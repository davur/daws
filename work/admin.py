from django.contrib import admin

from import_export.resources import ModelResource
from import_export.admin import ImportExportModelAdmin


from .models import *


class SkipUnchangedModelResource(ModelResource):
    class Meta:
        skip_unchanged = True
        report_skipped = False



class ClientResource(SkipUnchangedModelResource):
    class Meta:
        model = Client

@admin.register(Client)
class ClientAdmin(ImportExportModelAdmin):
    resource_class = ClientResource


class ProjectResource(SkipUnchangedModelResource):
    class Meta:
        model = Project

@admin.register(Project)
class ProjectAdmin(ImportExportModelAdmin):
    resource_class = ProjectResource
